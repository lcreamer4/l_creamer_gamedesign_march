//
//  GameOver.swift
//  L_Creamer_MagicMayhem
//
//  Created by Lara Creamer on 3/5/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit

var finalGameScore = 0

class GameOver: SKScene {
    
    let pixieDustAction: SKAction = SKAction.playSoundFileNamed("pixieDust.mp3", waitForCompletion: true)
    var finalMickey = 0
    var finalCoin = 0
    var multiplier = SKLabelNode(fontNamed: "Chalkduster")
    var totalLine = SKLabelNode(fontNamed: "Chalkduster")
 
    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "disneyBackground")
        background.zPosition = -1
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(background)
        self.userInteractionEnabled = true
        
        let replayGame = SKSpriteNode(imageNamed: "playAgainButton")
        replayGame.name = "replayGame"
        replayGame.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)/2.75)
        self.addChild(replayGame)
        self.userInteractionEnabled = true
        
        let mainMenu = SKSpriteNode(imageNamed: "mainMenuButton")
        mainMenu.name = "mainMenu"
        mainMenu.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)/1.8)
        self.addChild(mainMenu)
        self.userInteractionEnabled = true
        
        let gameOver = SKSpriteNode(imageNamed: "gameOver")
        gameOver.name = "gameOver"
        gameOver.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMaxY(self.frame)/1.1)
        self.addChild(gameOver)
        

        



        
        let mickeyScore = SKLabelNode(fontNamed: "Chalkduster")
        
        if variablePass.mickeyHitCount <= 10 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:         x1"
            finalMickey = variablePass.mickeyHitCount * 1
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 10 && variablePass.mickeyHitCount <= 20 {
           mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:          x2"
            finalMickey = variablePass.mickeyHitCount * 2
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 20 && variablePass.mickeyHitCount <= 30 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:          x3"
            finalMickey = variablePass.mickeyHitCount * 3
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 30 && variablePass.mickeyHitCount <= 40 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:           x4"
            finalMickey = variablePass.mickeyHitCount * 4
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 40 && variablePass.mickeyHitCount <= 50 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:            x5"
            finalMickey = variablePass.mickeyHitCount * 5
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 50 && variablePass.mickeyHitCount <= 60 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:             x6"
            finalMickey = variablePass.mickeyHitCount * 6
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 60 && variablePass.mickeyHitCount <= 70 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:             x7"
            finalMickey = variablePass.mickeyHitCount * 7
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 70 && variablePass.mickeyHitCount <= 80 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:             x8"
            finalMickey = variablePass.mickeyHitCount * 8
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 80 && variablePass.mickeyHitCount <= 90 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:             x9"
            finalMickey = variablePass.mickeyHitCount * 9
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 90 && variablePass.mickeyHitCount <= 100 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:            x10"
            finalMickey = variablePass.mickeyHitCount * 10
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 100 && variablePass.mickeyHitCount <= 110 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:             x11"
            finalMickey = variablePass.mickeyHitCount * 11
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 110 && variablePass.mickeyHitCount <= 120 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:              x12"
            finalMickey = variablePass.mickeyHitCount * 12
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 120 && variablePass.mickeyHitCount <= 130 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:              x13"
            finalMickey = variablePass.mickeyHitCount * 13
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 130 && variablePass.mickeyHitCount <= 140 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:              x14"
            finalMickey = variablePass.mickeyHitCount * 14
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 140 && variablePass.mickeyHitCount <= 150 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:              x15"
            finalMickey = variablePass.mickeyHitCount * 15
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 150 && variablePass.mickeyHitCount <= 160 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:              x16"
            finalMickey = variablePass.mickeyHitCount * 16
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 160 && variablePass.mickeyHitCount <= 170 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:              x17"
            finalMickey = variablePass.mickeyHitCount * 17
            print(finalMickey)
        }
        if variablePass.mickeyHitCount > 170 && variablePass.mickeyHitCount <= 180 {
            mickeyScore.text = ("Mickey Score:\(variablePass.mickeyHitCount)")
            multiplier.text = "Multipier:              x18"
            finalMickey = variablePass.mickeyHitCount * 18
            print(finalMickey)
        }
        

        mickeyScore.fontSize = 45
        mickeyScore.zPosition = 1
        mickeyScore.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMaxY(self.frame)/1.5)
        self.addChild(mickeyScore)
        
        totalLine.text = "____________________________________________________"
        totalLine.fontSize = 30
        totalLine.zPosition = 1
        totalLine.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMaxY(self.frame)/2)
        self.addChild(totalLine)
        
        multiplier.fontSize = 30
        multiplier.zPosition = 1
        multiplier.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMaxY(self.frame)/1.6)
        self.addChild(multiplier)
    
        let coinScore = SKLabelNode(fontNamed: "Chalkduster")
        coinScore.text = ("Coin Score:\(variablePass.mickeyCoinHit * 10)")
        finalCoin = variablePass.mickeyCoinHit * 10
        coinScore.fontSize = 45
        coinScore.zPosition = 3
        coinScore.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMaxY(self.frame)/1.85)
        self.addChild(coinScore)
        
        let finalScore = SKLabelNode(fontNamed: "Chalkduster")
        finalScore.text = ("Final Score:\(finalMickey + finalCoin)")
        finalScore.fontSize = 50
        finalScore.zPosition = 1
        finalScore.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMaxY(self.frame)/2.5)
        self.addChild(finalScore)
        
        let finalNumScore = (finalMickey + finalCoin)
        
        finalGameScore = finalNumScore
        
        if (finalGameScore > previousHighScore) {
            self.saveHighScore("MMLEADERBOARD", score: finalGameScore)
            previousHighScore = finalGameScore
            
            defaults.setInteger(finalGameScore, forKey: "previousHighScore")
        }
        
        if finalGameScore == 0 {
            incrementCurrentPercentOfAchievement("lowestScore", amount: 100)
        }
        

    }
    
    //Create the function that will load the percentages for the users achievement.
    func loadAchievementPercentages() {
        print("Getting percent of past achievments.")
        //This piece will load the achievement percentage.
        GKAchievement.loadAchievementsWithCompletionHandler({ (allAchievements, error) -> Void in
            //if there is an error, it will be handled in a print statement.
            if error != nil {
                print("Game center could not load achiements with error \(error)")
            } else {
                //If there is no error, continue to load the achievements.
                //This could be nil if there is no progress on any achievments thus far.
                if (allAchievements != nil) {
                    //For each achievement created in the apple developer account...
                    for theAchievement in allAchievements! {
                        //Assign each achievement to be their own achievement, and therefore display them separately.
                        if let singleAchievement:GKAchievement = theAchievement {
                            gameCenterAchievments[singleAchievement.identifier!] = singleAchievement
                        }
                    }
                    //For each individual ID and achievement in the apple developer account...
                    for (id, achievement) in gameCenterAchievments {
                        //Print statements for each achievement along with whether they are complete.
                        print("\(id) - \(achievement.percentComplete)")
                    }
                }
            }
        })
    }
    
    //Create the function that will change the current percentage of achievement.
    func incrementCurrentPercentOfAchievement(identifier: String, amount: Double) {
        
        //Check to make sure the user is signed into Game Center, if they are....
        if GKLocalPlayer.localPlayer().authenticated {
            
            //Create a variable for the current percent.
            var currentPercentFound: Bool = false
            
            //If there are currently achievements available...
            if (gameCenterAchievments.count != 0) {
                //For each ID and achievement in the Game Center Achievements...
                for (id, achievement) in gameCenterAchievments {
                    //if the ID is equal to the identifier...
                    if (id == identifier) {
                        //Change the current percent found to true.
                        currentPercentFound = true
                        //Create a varaiable that will represent the current percentage of an achievement.
                        var currentPercent: Double = achievement.percentComplete
                        //Assign the current percent equal to itself plus an amount.
                        currentPercent = currentPercent + amount
                        //Call the report achievement function.
                        reportAchievement(identifier, percentComplete: currentPercent)
                        
                        break
                        
                    }
                }
            }
            
            //If there is no current percentage found...
            if (currentPercentFound == false) {
                //Call the report achievement function.
                reportAchievement(identifier, percentComplete: amount)
                
            }
        }
    }
    
    //Create the function that will report the achievement and percentage to the leaderboard
    func reportAchievement(identifier: String, percentComplete: Double) {
        //Create a variable that will represent each achievement.
        let achievement = GKAchievement(identifier: identifier)
        //Use the achievement plus the percentage complete of that achievement and assign it to the varable percent complete.
        achievement.percentComplete = percentComplete
        //Create an achievement array which will have all achievements created for the game.
        let achievementArray: [GKAchievement] = [achievement]
        //Report each achievement to the game center.
        GKAchievement.reportAchievements(achievementArray) { (error) -> Void in
            //if there is an error...
            if (error != nil) {
                //Print out the error.
                print(error)
            } else {
                //Otherwise print out the achievement and percentage.
                print("Reported Achievement with percentage complete of \(percentComplete)")
                gameCenterAchievments.removeAll()
                //Then remove and reload all achievements and percentages for the game.
                self.loadAchievementPercentages()
            }
        }
    }
    
    //Create a function that will save the high score of the game.
    func saveHighScore(indentifier: String, score: Int) {
        if (GKLocalPlayer.localPlayer().authenticated) {
            
            let scoreReporter = GKScore(leaderboardIdentifier: "MMLEADERBOARD")
            scoreReporter.value = Int64(score)
            let scoreArray:[GKScore] = [scoreReporter]
            
            GKScore.reportScores(scoreArray, withCompletionHandler: {
                error -> Void in
                if error != nil {
                    print("error")
                } else {
                    print("posted score of \(score)")
                }
            })
            
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        let touch = touches.first! as UITouch
        let location = touch.locationInNode(self)
        let touchNode = self.nodeAtPoint(location)
        if touchNode.name == "replayGame" {
            //Add in the functionality to open the actual game scene.
            variablePass.mickeyCoinHit = 0
            variablePass.mickeyHitCount = 0
            if levelChosen == "easy" {
                let gameScene = easyLevelScene(size: self.size)
                let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
                gameScene.scaleMode = SKSceneScaleMode.AspectFill
                self.scene!.view?.presentScene(gameScene, transition: transition)
            }
            if levelChosen == "medium" {
                let gameScene = PlayScene(size: self.size)
                let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
                gameScene.scaleMode = SKSceneScaleMode.AspectFill
                self.scene!.view?.presentScene(gameScene, transition: transition)
            }
            if levelChosen == "hard"  {
                let gameScene = hardLevelScene(size: self.size)
                let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
                gameScene.scaleMode = SKSceneScaleMode.AspectFill
                self.scene!.view?.presentScene(gameScene, transition: transition)
            }
            
            runAction(pixieDustAction)
            
        }
        
        if touchNode.name == "mainMenu" {
            variablePass.mickeyHitCount = 0
            variablePass.mickeyCoinHit = 0
            //Add in the functionality to open the actual game scene.
            let gameScene = GameScene(size: self.size)
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
            runAction(clickAction)
            levelChosen = ""
            
        }
    }
    
//    override func update(currentTime: NSTimeInterval) {
//        if finalGameScore == 0 {
//            incrementCurrentPercentOfAchievement("lowestScore", amount: 100)
//        }
//    }
}



