//
//  PauseScene.swift
//  L_Creamer_MagicMayhem
//
//  Created by Lara Creamer on 3/15/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import SpriteKit

class PauseScene: SKScene {

    let pixieDustAction: SKAction = SKAction.playSoundFileNamed("pixieDust.mp3", waitForCompletion: true)
    
    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "disneyBackground")
        background.zPosition = -1
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(background)
        
        let replayGame = SKSpriteNode(imageNamed: "continueGameButton")
        replayGame.name = "continueGame"
        replayGame.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(replayGame)
        self.userInteractionEnabled = true
        
        let gamePausedLogo = SKSpriteNode(imageNamed: "GamePausedLogo")
        gamePausedLogo.name = "GamePaused"
        gamePausedLogo.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMaxY(self.frame)/1.2)
        self.addChild(gamePausedLogo)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        let touch = touches.first! as UITouch
        let location = touch.locationInNode(self)
        let touchNode = self.nodeAtPoint(location)
        if touchNode.name == "continueGame" {
        let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
        let gameScene = PlayScene(size: self.size)
        gameScene.scaleMode = SKSceneScaleMode.AspectFill
        self.scene!.view?.presentScene(gameScene, transition: transition)
            
            }
        }
}
