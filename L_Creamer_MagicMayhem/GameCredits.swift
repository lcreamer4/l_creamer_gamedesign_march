//
//  GameCredits.swift
//  L_Creamer_MagicMayhem
//
//  Created by Lara Creamer on 3/18/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import SpriteKit

class GameCredits: SKScene {
    
    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "gameCreditsBackground")
        background.zPosition = -1
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(background)
        self.userInteractionEnabled = true
        
        let mainMenu = SKSpriteNode(imageNamed: "mainMenuButton")
        mainMenu.name = "mainMenu"
        mainMenu.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)/2)
        self.addChild(mainMenu)
        self.userInteractionEnabled = true
        
    }
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first! as UITouch
        let location = touch.locationInNode(self)
        let touchNode = self.nodeAtPoint(location)
        if touchNode.name == "mainMenu" {
            
        //Add in the functionality to open the actual game scene.
        let gameScene = GameScene(size: self.size)
        let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
        gameScene.scaleMode = SKSceneScaleMode.AspectFill
        self.scene!.view?.presentScene(gameScene, transition: transition)
        runAction(clickAction)
        }
    }
}
