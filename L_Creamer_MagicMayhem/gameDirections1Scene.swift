//
//  gameDirections1Scene.swift
//  L_Creamer_MagicMayhem
//
//  Created by Lara Creamer on 3/17/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import SpriteKit

class gameDirections1Scene: SKScene {
    
     override func didMoveToView(view: SKView) {
    
    let background = SKSpriteNode(imageNamed: "directions1")
    background.zPosition = -1
    background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
    self.addChild(background)
        
    let nextButton = SKSpriteNode(imageNamed: "nextButton")
    nextButton.name = "next"
    nextButton.xScale = 0.65
    nextButton.yScale = 0.65
    nextButton.zPosition = 1
    nextButton.position = CGPoint(x: CGRectGetMaxX(self.frame)/1.6, y: CGRectGetMidY(self.frame)/2.7)
    self.addChild(nextButton)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        let touch = touches.first! as UITouch
        let location = touch.locationInNode(self)
        let touchNode = self.nodeAtPoint(location)
        if touchNode.name == "next" {
        let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
        let gameScene = gameDirections2Scene(size: self.size)
        gameScene.scaleMode = SKSceneScaleMode.AspectFill
        self.scene!.view?.presentScene(gameScene, transition: transition)
        self.runAction(clickAction)
            }
        }
}

