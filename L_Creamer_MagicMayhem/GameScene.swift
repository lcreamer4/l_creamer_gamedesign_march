//
//  GameScene.swift
//  L_Creamer_MagicMayhem
//
//  Created by Lara Creamer on 3/3/16.
//  Copyright (c) 2016 Lara Creamer. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit

var easyLevel = false
var mediumLevel = false
var hardLevel = false
var levelChosen = ""
var previousHighScore = 0
let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
var gameCenterAchievments = [String: GKAchievement]()


let clickAction: SKAction = SKAction.playSoundFileNamed("buttonClick.mp3", waitForCompletion: false)

class GameScene: SKScene, GKGameCenterControllerDelegate {
    
    override func didMoveToView(view: SKView) {
        
        previousHighScore = defaults.integerForKey("previousHighScore")
        
        authenticateLocalPlayer()
        
        let background = SKSpriteNode(imageNamed: "disneyBackground")
        background.zPosition = -1
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(background)
        self.userInteractionEnabled = true
        
        let newEasyGame = SKSpriteNode(imageNamed: "newGame_easy")
        newEasyGame.name = "newGame_easy"
        newEasyGame.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMaxY(self.frame)/1.67)
        self.addChild(newEasyGame)
        self.userInteractionEnabled = true
        
        let newMedGame = SKSpriteNode(imageNamed: "newGame_medium")
        newMedGame.name = "newGame_medium"
        newMedGame.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(newMedGame)
        self.userInteractionEnabled = true
        
        let newHardGame = SKSpriteNode(imageNamed: "newGame_hard")
        newHardGame.name = "newGame_hard"
        newHardGame.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)/1.25)
        self.addChild(newHardGame)
        self.userInteractionEnabled = true
        
        let gameCredits = SKSpriteNode(imageNamed: "gameCredits")
        gameCredits.name = "gameCredits"
        gameCredits.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)/2.45)
        self.addChild(gameCredits)
        self.userInteractionEnabled = true
        
        let leaderboard = SKSpriteNode(imageNamed: "leaderboardButton")
        leaderboard.name = "leaderboard"
        leaderboard.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)/1.65)
        self.addChild(leaderboard)
        self.userInteractionEnabled = true
        
        let gameLogo = SKSpriteNode(imageNamed: "mMLogo")
        gameLogo.name = "mMLogo"
        gameLogo.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMaxY(self.frame)/1.1)
        self.addChild(gameLogo)
    }
    
    func authenticateLocalPlayer() {
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = { (viewController, error) -> Void in
            if viewController != nil {
                let vc: UIViewController = self.view!.window!.rootViewController!
                vc.presentViewController(viewController!, animated: true, completion: nil)
            } else {
                print("Authentication is \(GKLocalPlayer.localPlayer().authenticated)")
                //Do somthing based on the player being logged in.
                
                gameCenterAchievments.removeAll()
                self.loadAchievementPercentages()
            }
        }
    }
    
    func loadAchievementPercentages() {
        print("Getting percent of past achievments.")
        GKAchievement.loadAchievementsWithCompletionHandler({ (allAchievements, error) -> Void in
            if error != nil {
                print("Gsme center could not load achiements with error \(error)")
            } else {
                //This could be nil if there is no progress on any achievments thus far.
                if (allAchievements != nil) {
                    
                    for theAchievement in allAchievements! {
                        if let singleAchievement:GKAchievement = theAchievement {
                            gameCenterAchievments[singleAchievement.identifier!] = singleAchievement
                        }
                    }
                    for (id, achievement) in gameCenterAchievments {
                        print("\(id) - \(achievement.percentComplete)")
                    }
                }
            }
        })
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        let touch = touches.first! as UITouch
        let location = touch.locationInNode(self)
        let touchNode = self.nodeAtPoint(location)
        if touchNode.name == "newGame_easy" {
            easyLevel = true
            //Add in the functionality to open the actual game scene.
            let gameScene = gameDirections1Scene(size: self.size)
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
            self.runAction(clickAction)
            
        }
        if touchNode.name == "newGame_medium" {
            mediumLevel = true
            //Add in the functionality to open the actual game scene.
            let gameScene = gameDirections1Scene(size: self.size)
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
            self.runAction(clickAction)
            
        }
        if touchNode.name == "newGame_hard" {
            hardLevel = true
            //Add in the functionality to open the actual game scene.
            let gameScene = gameDirections1Scene(size: self.size)
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
            self.runAction(clickAction)
        }
        
        if touchNode.name == "leaderboard" {
            
            showGameCenter()
        }

        if touchNode.name == "gameCredits" {
            //Add in the functionality to open the actual game scene.
            let gameScene = GameCredits(size: self.size)
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
            self.runAction(clickAction)
        }
    }
    
    func incrementCurrentPercentOfAchievement(identifier: String, amount: Double) {
        
        if GKLocalPlayer.localPlayer().authenticated {
            
            var currentPercentFound: Bool = false
            
            if (gameCenterAchievments.count != 0) {
                for (id, achievement) in gameCenterAchievments {
                    if (id == identifier) {
                        
                        currentPercentFound = true
                        
                        var currentPercent: Double = achievement.percentComplete
                        
                        currentPercent = currentPercent + amount
                        
                        reportAchievement(identifier, percentComplete: currentPercent)
                        
                        break
                        
                    }
                }
            }
            
            if (currentPercentFound == false) {
                reportAchievement(identifier, percentComplete: amount)
                
            }
        }
    }
    
    func reportAchievement(identifier: String, percentComplete: Double) {
        
        let achievement = GKAchievement(identifier: identifier)
        
        achievement.percentComplete = percentComplete
        
        let achievementArray: [GKAchievement] = [achievement]
        
        GKAchievement.reportAchievements(achievementArray) { (error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                print("Reported Achievement with percentage complete of \(percentComplete)")
                gameCenterAchievments.removeAll()
                self.loadAchievementPercentages()
            }
        }
    }
    

    
    override func update(currentTime: NSTimeInterval) {
        
    }
    
    func showGameCenter() {
        let gameCenterViewController = GKGameCenterViewController()
        gameCenterViewController.gameCenterDelegate = self
        
        let vc:UIViewController = self.view!.window!.rootViewController!
        vc.presentViewController(gameCenterViewController, animated: true, completion: nil)
    }
    
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
        gameCenterAchievments.removeAll()
        loadAchievementPercentages()
    }
    
    
    
}
