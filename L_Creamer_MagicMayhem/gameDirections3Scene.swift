//
//  gameDirections3Scene.swift
//  L_Creamer_MagicMayhem
//
//  Created by Lara Creamer on 3/17/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import SpriteKit

class gameDirections3Scene: SKScene {
    
    let pixieDustAction: SKAction = SKAction.playSoundFileNamed("pixieDust.mp3", waitForCompletion: true)
    var gameScene = SKScene()
    
    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "directions3")
        background.zPosition = -1
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(background)
        
        let nextButton = SKSpriteNode(imageNamed: "playGameButton")
        nextButton.name = "play"
        nextButton.zPosition = 1
        nextButton.xScale = 0.65
        nextButton.yScale = 0.65
        nextButton.position = CGPoint(x: CGRectGetMaxX(self.frame)/1.6, y: CGRectGetMidY(self.frame)/2.7)
        self.addChild(nextButton)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        let touch = touches.first! as UITouch
        let location = touch.locationInNode(self)
        let touchNode = self.nodeAtPoint(location)
        if touchNode.name == "play" {
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            if easyLevel == true {
            gameScene = easyLevelScene(size: self.size)
            }
            if mediumLevel == true {
                gameScene = PlayScene(size: self.size)
            }
            if hardLevel == true {
                gameScene = hardLevelScene(size: self.size)
            }
            
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
            self.scene?.runAction(pixieDustAction)
        }
    }
}




