//
//  GameScene1.swift
//  L_Creamer_MagicMayhem
//
//  Created by Lara Creamer on 3/6/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import SpriteKit

class GameScene1: SKScene {
    var lastUpdateTime: CFTimeInterval = 0.0
    var lastSpawnTime: CFTimeInterval = 0.0
    var lastObstacleSpawnTime: CFTimeInterval = 0.0
    var lastCoinSpawnTime: CFTimeInterval = 0.0
    
    let pixieDustAction: SKAction = SKAction.playSoundFileNamed("pixieDust.mp3", waitForCompletion: true)
    let coinAction: SKAction = SKAction.playSoundFileNamed("coin.mp3", waitForCompletion: true)
    let songAction: SKAction = SKAction.playSoundFileNamed("songTest.mp3", waitForCompletion: true)
    
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        let background = SKSpriteNode(imageNamed: "disneyBackground")
        background.zPosition = -1
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(background)
        self.userInteractionEnabled = true
        
        let border = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        self.physicsWorld.gravity = CGVectorMake(0.0, 0.0)
        //Set the physics body of the world to the border just created above.
        self.physicsBody = border
        self.physicsWorld.contactDelegate = self
        
        runAction(songAction)
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody = contact.bodyA.node
        var secondBody = contact.bodyB.node
        
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
            firstBody = contact.bodyA.node
            secondBody = contact.bodyB.node
        } else {
            firstBody = contact.bodyB.node
            secondBody = contact.bodyA.node
        }
        if firstBody!.name == physicsCategories.mickeyName && secondBody!.name == physicsCategories.mickeyObstacleName {
            print("Hit Obstacle")
            //This is where you will test the hit of your two objects and if they hit, then take away coins, and if there are no more coins to take away, then end the game. Also remove the mickey from the screen.
            firstBody?.removeFromParent()
            secondBody?.removeFromParent()
            
        }
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        let touch = touches.first! as UITouch
        let location = touch.locationInNode(self)
        let touchNode = self.nodeAtPoint(location)
        if touchNode.name == "key" {
            touchNode.removeFromParent()
            //Add a sound when a mickey is removed.
            //            runAction(pixieDustAction)
        }
        if touchNode.name == "mickeyCoin" {
            touchNode.removeFromParent()
            //Add a sound when a coin is removed.
            runAction(coinAction)
            //Add one to the players coin score.
        }
    }
    
    //This function is somthing that I can use in my game
    func spawnMickey() {
        //Create the sprite, which will be the mickeys.
        let sprite: SKSpriteNode = SKSpriteNode(imageNamed: "mickey")
        sprite.name = physicsCategories.mickeyName
        //Because the image is too big for multiple to fit on the screen, shrk the x and y scale to be smaller.
        sprite.xScale = 0.1
        sprite.yScale = 0.1
        //Assign the position of z, or how far back it is, to 1 so that it is not part of the background.
        sprite.zPosition = 1
        
        //This is where you will assign where the mickey appears on the screen? Change the axis that is randomized from the y axis to the x axis.
        
        var xCoords: CGFloat = 0.0
        
        let maxX = CGRectGetMaxX(self.frame)
        let maxY = CGRectGetMaxY(self.frame)
        
        let randomXCoords = [maxX, maxX/2, maxX/3]
        let selectedCoords = arc4random_uniform(UInt32(randomXCoords.count))
        
        if selectedCoords == 0 {
            xCoords = maxX/1.5
        }
        if selectedCoords == 1 {
            xCoords = maxX/2
        }
        if selectedCoords == 2 {
            xCoords = maxX/3
        }
        
        //% = remainder.
        
        //Assign the starting point of the sprite to the top center of the frame.
        sprite.position = CGPointMake(xCoords, maxY)
        
        //Add in physics for the mickey
        sprite.physicsBody = SKPhysicsBody(circleOfRadius: sprite.size.height/2)
        //This is the friction that will affect your object.
        sprite.physicsBody?.friction = 0.0
        //This is the amount of bounciness your object will have.
        sprite.physicsBody?.restitution = 0.0
        //This is how much your object will slow down over time.
        sprite.physicsBody?.linearDamping = 0.0
        //Defines which category this mask belongs to.
        sprite.physicsBody?.categoryBitMask = physicsCategories.mickeyCategory
        //Defines which object or objects this mask interacts with.
        sprite.physicsBody?.contactTestBitMask = physicsCategories.mickeyObstacleCategory
        
        
        //Add the sprite to the scene.
        self.addChild(sprite)
        
        //Create a duration so that the mickeys come aat a constant pace.
        let duration = 2.0
        
        //Move the sprite from the point at the top of the screen to the point at the bottom of the screen.
        let actionMove: SKAction = SKAction.moveTo(CGPointMake(xCoords, -100), duration: duration)
        
        //When the mickey has moved across the screen completely, remove it to save space.
        let done: SKAction = SKAction.removeFromParent()
        
        //Run the action.
        sprite.runAction(SKAction.sequence([actionMove, done]))
        
    }
    
    func spawnCoin() {
        //Create the sprite, which will be the mickey coins.
        let coinSprite: SKSpriteNode = SKSpriteNode(imageNamed: "mickeyCoin")
        coinSprite.name = "mickeyCoin"
        //Because the image is too big for multiple to fit on the screen, shrink the x and y scale to be smaller.
        coinSprite.xScale = 0.2
        coinSprite.yScale = 0.2
        //Assign the position of z, or how far back it is, to 1 so that it is not part of the background.
        coinSprite.zPosition = 1
        
        //This is where you will assign where the mickey coin appears on the screen? Change the axis that is randomized from the y axis to the x axis.
        
        var xCoords: CGFloat = 0.0
        
        let maxX = CGRectGetMaxX(self.frame)
        let maxY = CGRectGetMaxY(self.frame)
        
        let randomXCoords = [maxX/1.72, maxX/2.5]
        
        let selectedXCoords = arc4random_uniform(UInt32(randomXCoords.count))
        
        if selectedXCoords == 0 {
            xCoords = maxX/1.72
        }
        if selectedXCoords == 1 {
            xCoords = maxX/2.5
        }
        
        //% = remainder.
        
        //Assign the starting point of the sprite to the top center of the frame.
        coinSprite.position = CGPointMake(xCoords, maxY)
        
        //Add the sprite to the scene.
        self.addChild(coinSprite)
        
        //Create a duration so that the mickeys come at a constant pace.
        let duration = 2.0
        
        //Move the sprite from the point at the top of the screen to the point at the bottom of the screen.
        let actionMove: SKAction = SKAction.moveTo(CGPointMake(xCoords,-100), duration: duration)
        
        //When the mickey has moved across the screen completely, remove it to save space.
        let done: SKAction = SKAction.removeFromParent()
        
        //Run the action.
        coinSprite.runAction(SKAction.sequence([actionMove, done]))
        
    }
    
    func spawnObstacle() {
        //Create the sprite, which will be the bombs.
        let obstacleSprite: SKSpriteNode = SKSpriteNode(imageNamed: "mickeyObstacle")
        
        //Because the image is too big for multiple to fit on the screen, shrink the x and y scale to be smaller.
        obstacleSprite.xScale = 0.065
        obstacleSprite.yScale = 0.065
        //Assign the position of z, or how far back it is, to 1 so that it is not part of the background.
        obstacleSprite.zPosition = 1
        
        //This is where you will assign where the mickey bomb appears on the screen? Change the axis that is randomized from the y axis to the x axis.
        
        var xCoords: CGFloat = 0.0
        var yCoords: CGFloat = 0.0
        
        let maxX = CGRectGetMaxX(self.frame)
        let maxY = CGRectGetMaxY(self.frame)
        
        let randomXCoords = [maxX, maxX/2, maxX/3]
        let randomYCoords = [maxY/2, maxY/3, maxY/4]
        
        let selectedXCoords = arc4random_uniform(UInt32(randomXCoords.count))
        let selectedYCoords = arc4random_uniform(UInt32(randomYCoords.count))
        
        if selectedXCoords == 0 {
            xCoords = maxX
        }
        if selectedXCoords == 1 {
            xCoords = maxX/2
        }
        if selectedXCoords == 2 {
            xCoords = maxX/3
        }
        
        
        if selectedYCoords == 0 {
            yCoords = maxY/2
        }
        if selectedYCoords == 1 {
            yCoords = maxY/3
        }
        if selectedYCoords == 2 {
            yCoords = maxY/4
        }
        
        //% = remainder.
        
        //Assign the starting point of the sprite to the top center of the frame.
        obstacleSprite.position = CGPointMake(xCoords, yCoords)
        
        obstacleSprite.name = physicsCategories.mickeyObstacleName
        
        //Add the sprite to the scene.
        self.addChild(obstacleSprite)
        
        obstacleSprite.physicsBody = SKPhysicsBody(rectangleOfSize: obstacleSprite.frame.size)
        obstacleSprite.physicsBody?.restitution = 0.1
        obstacleSprite.physicsBody?.friction = 0.4
        //The paddle is not dynamic because it does not move when somthing hits it.
        obstacleSprite.physicsBody?.dynamic = false
        obstacleSprite.physicsBody?.categoryBitMask = physicsCategories.mickeyObstacleCategory
        
        //Create a duration so that the mickeys come at a constant pace.
        let duration = 4.0
        
        //Move the sprite from the point at the top of the screen to the point at the bottom of the screen.
        let actionMove: SKAction = SKAction.moveTo(CGPointMake(xCoords,yCoords), duration: duration)
        
        //When the mickey has moved across the screen completely, remove it to save space.
        let done: SKAction = SKAction.removeFromParent()
        
        //Run the action.
        obstacleSprite.runAction(SKAction.sequence([actionMove, done]))
        
    }
    
    
    //Create an update function to save space.
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        var timeSinceLast: CFTimeInterval = currentTime - self.lastUpdateTime
        self.lastUpdateTime = currentTime
        
        if (timeSinceLast > 1) {
            timeSinceLast = 1.0/60.0
        }
        
        self.updateWithTimeSinceLast(timeSinceLast)
    }
    
    func updateWithTimeSinceLast(timeSinceLast: CFTimeInterval) {
        self.lastSpawnTime += timeSinceLast
        self.lastObstacleSpawnTime += timeSinceLast
        self.lastCoinSpawnTime += timeSinceLast
        
        if (self.lastSpawnTime > 0.55) {
            self.lastSpawnTime = 0
            self.spawnMickey()
            
        }
        if (self.lastCoinSpawnTime > 3) {
            self.lastCoinSpawnTime = 0
            self.spawnCoin()
        }
        
        if (self.lastObstacleSpawnTime > 6) {
            self.lastObstacleSpawnTime = 0
            self.spawnObstacle()
        }
    }
}



