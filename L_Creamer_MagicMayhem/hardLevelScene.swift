//
//  hardLevelScene.swift
//  L_Creamer_MagicMayhem
//
//  Created by Lara Creamer on 3/17/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation
import GameKit



class hardLevelScene: SKScene, SKPhysicsContactDelegate {
    //Set up all of the variables for the beginning of the game.
    var backgroundMusic: SKAudioNode!
    var lastUpdateTime: CFTimeInterval = 0.0
    var lastSpawnTime: CFTimeInterval = 0.0
    var lastObstacleSpawnTime: CFTimeInterval = 0.0
    var lastCoinSpawnTime: CFTimeInterval = 0.0
    var lastCheckPauseButton: CFTimeInterval = 0.0
    var mickeySpawnCount = 0
    var globalLabel = SKLabelNode(fontNamed:"Chalkduster")
    var globalKeyNumber = SKLabelNode(fontNamed: "Chalkduster")
    var pauseButton = SKSpriteNode(imageNamed: "pauseButton")
    var pauseButtonClicked = false
    var bottomBar = SKSpriteNode(imageNamed: "topOfGame")
    
    //Create the coin animation.
    let coinAnimatedAction: SKAction = SKAction.animateWithTextures([
        SKTexture(imageNamed: "mickeyAnimation1"),
        SKTexture(imageNamed: "mickeyAnimation1"),
        SKTexture(imageNamed: "mickeyAnimation2"),
        SKTexture(imageNamed: "mickeyAnimation3"),
        SKTexture(imageNamed: "mickeyAnimation4"),
        SKTexture(imageNamed: "mickeyAnimation5"),
        SKTexture(imageNamed: "mickeyAnimation6"),
        SKTexture(imageNamed: "mickeyAnimation7"),
        SKTexture(imageNamed: "mickeyAnimation8"),
        SKTexture(imageNamed: "mickeyAnimation9"),
        SKTexture(imageNamed: "mickeyAnimation10")], timePerFrame:
        0.03)
    
    //Create the animation that will explode the mickey keys.
    let explosionAction: SKAction = SKAction.animateWithTextures([
        SKTexture(imageNamed: "mickeyAnimation1"),
        SKTexture(imageNamed: "mickeyAnimation2"),
        SKTexture(imageNamed: "mickeyAnimation3"),
        SKTexture(imageNamed: "mickeyAnimation4"),
        SKTexture(imageNamed: "mickeyAnimation5"),
        SKTexture(imageNamed: "mickeyAnimation6"),
        SKTexture(imageNamed: "mickeyAnimation7"),
        SKTexture(imageNamed: "mickeyAnimation8"),
        SKTexture(imageNamed: "mickeyAnimation9"),
        SKTexture(imageNamed: "mickeyAnimation10")], timePerFrame:
        0.03)
    
    //Create the coin added action.
    let coinAddedAction: SKAction = SKAction.animateWithTextures([
        SKTexture(imageNamed: "coinAnimation1"),
        SKTexture(imageNamed: "coinAnimation2"),
        SKTexture(imageNamed: "coinAnimation3"),
        SKTexture(imageNamed: "coinAnimation4"),
        SKTexture(imageNamed: "coinAnimation5"),
        SKTexture(imageNamed: "coinAnimation6"),
        SKTexture(imageNamed: "coinAnimation7"),
        SKTexture(imageNamed: "coinAnimation8"),
        SKTexture(imageNamed: "coinAnimation9"),
        SKTexture(imageNamed: "coinAnimation10"),
        SKTexture(imageNamed: "coinAnimation10"),
        SKTexture(imageNamed: "coinAnimation11"),
        SKTexture(imageNamed: "coinAnimation12"),
        SKTexture(imageNamed: "coinAnimation13")
        ], timePerFrame: 0.05)
    
    //Set the sound effects for the coin as well as the background music.
    let coinAction: SKAction = SKAction.playSoundFileNamed("coin.mp3", waitForCompletion: true)
    let songAction: SKAction = SKAction.playSoundFileNamed("hardBackgroundMusic.mp3", waitForCompletion: false)
    
    //Create the audio player.
    var player = AVAudioPlayer!()
    
    //Create the function for the timer.
    func timerAction() {
        ++timerCounter
        print("\(timerCounter) seconds")
    }
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        //Asssign what level was chosen.
        levelChosen = "hard"
        
        //Assign the global text to the number of coins tapped.
        globalLabel.text = String(variablePass.mickeyCoinHit)
        globalLabel.fontSize = 25
        globalLabel.zPosition = 3
        globalLabel.position = CGPoint(x:CGRectGetMaxX(self.frame)/1.45, y:CGRectGetMaxY(self.frame)/1.05)
        self.addChild(globalLabel)
        
        //Assign the global key number to the number of spawned mickeys out of the total number of mickeys in the level.
        globalKeyNumber.text = "\(mickeySpawnCount)/175"
        globalKeyNumber.fontSize = 25
        globalKeyNumber.zPosition = 3
        globalKeyNumber.position = CGPoint(x: CGRectGetMaxX(self.frame)/2, y: CGRectGetMaxY(self.frame)/1.05)
        self.addChild(globalKeyNumber)
        
        //Create the sprite that will represent the number of coins collected in the upper right hadn corner.
        let staticCoinSprite: SKSpriteNode = SKSpriteNode(imageNamed: "mickeyCoin")
        staticCoinSprite.name = "coinKey"
        //Because the image is too big for multiple to fit on the screen, shrink the x and y scale to be smaller.
        staticCoinSprite.xScale = 0.11
        staticCoinSprite.yScale = 0.11
        //Assign the position of z, or how far back it is, to 1 so that it is not part of the background.
        staticCoinSprite.zPosition = 3
        staticCoinSprite.position = CGPoint(x: CGRectGetMaxX(self.frame)/1.55, y: CGRectGetMaxY(self.frame)/1.035)
        self.addChild(staticCoinSprite)
        
        //Assign the background of the game.
        let background = SKSpriteNode(imageNamed: "disneyBackground")
        background.zPosition = -1
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(background)
        self.userInteractionEnabled = true
        
        //Assign the top of the game, to hold the global coin variable, global key variable, and the pause button.
        let topOfGame = SKSpriteNode(imageNamed: "topOfGame")
        topOfGame.name = "topOfGame"
        topOfGame.zPosition = 2
        topOfGame.position = CGPoint(x: CGRectGetMidX(self.frame)/1, y: CGRectGetMaxY(self.frame)/1.03)
        self.addChild(topOfGame)
        
        //Assign the position and size of the pause button.
        pauseButton.name = "pauseButton"
        pauseButton.xScale = 0.3
        pauseButton.yScale = 0.3
        //Assign the position of z, or how far back it is, to 1 so that it is not part of the background.
        pauseButton.zPosition = 3
        pauseButton.position = CGPoint(x: CGRectGetMaxX(self.frame)/3, y: CGRectGetMaxY(self.frame)/1.035)
        self.addChild(pauseButton)
        
        bottomBar.zPosition = 1
        bottomBar.name = physicsCategories.bottomObstacleName
        bottomBar.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMinY(self.frame) - 5)
        bottomBar.physicsBody = SKPhysicsBody(rectangleOfSize: bottomBar.frame.size)
        bottomBar.physicsBody?.restitution = 0.1
        bottomBar.physicsBody?.friction = 0.4
        //The paddle is not dynamic because it does not move when somthing hits it.
        bottomBar.physicsBody?.dynamic = false
        bottomBar.physicsBody?.categoryBitMask = physicsCategories.bottomObstacleCategory
        self.addChild(bottomBar)
        
        
        //Create the phyaics for the bounds of the game.
        let border = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        self.physicsWorld.gravity = CGVectorMake(0.0, 0.0)
        //Set the physics body of the world to the border just created above.
        self.physicsBody = border
        self.physicsWorld.contactDelegate = self
        
        //Create and set the background music for the game.
        let soundFilePath = NSBundle.mainBundle().pathForResource("hardBackgroundMusic.mp3", ofType: nil)
        let soundFileURL = NSURL(fileURLWithPath: soundFilePath!)
        do
        {
            player = try AVAudioPlayer(contentsOfURL: soundFileURL, fileTypeHint: nil)
        }
        catch let error as NSError
        {
            print(error.description)
        }
        player.numberOfLoops = 0
        
        if pauseButtonClicked == false {
            print("Music")
            player.play()
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "timerAction", userInfo: nil, repeats: true)
            timerCounter = 0
            timerAction()
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody = contact.bodyA.node
        var secondBody = contact.bodyB.node
        
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
            firstBody = contact.bodyA.node
            secondBody = contact.bodyB.node
        } else {
            firstBody = contact.bodyB.node
            secondBody = contact.bodyA.node
        }
        if firstBody!.name == physicsCategories.mickeyName && secondBody!.name == physicsCategories.mickeyObstacleName {
            print("Hit Obstacle")
            //This is where you will test the hit of your two objects and if they hit, then take away coins, and if there are no more coins to take away, then end the game. Also remove the mickey from the screen.
            firstBody?.removeFromParent()
            secondBody?.removeFromParent()
            self.removeAllActions()
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            let gameScene = GameOver(size: self.size)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            player.stop()
            self.scene!.view?.presentScene(gameScene, transition: transition)
        }
        
        if firstBody!.name == physicsCategories.mickeyName && secondBody!.name == physicsCategories.bottomObstacleName {
            print("Hit Bottom")
            //This is where you will test the hit of your two objects and if they hit, then take away coins, and if there are no more coins to take away, then end the game. Also remove the mickey from the screen.
            firstBody?.removeFromParent()
            secondBody?.removeFromParent()
            self.removeAllActions()
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            let gameScene = GameOver(size: self.size)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            player.stop()
            timer.invalidate()
            self.scene!.view?.presentScene(gameScene, transition: transition)
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        let touch = touches.first! as UITouch
        let location = touch.locationInNode(self)
        let touchNode = self.nodeAtPoint(location)
        //Check to see if the touch is on a mickey
        if touchNode.name == "key" {
            variablePass.mickeyHitCount++
            touchNode.runAction(SKAction.sequence([explosionAction, SKAction.removeFromParent()]))
        }
        
        //Assign what happens when a coin is tapped.
        //Check to see if the touch is on a coin.
        if touchNode.name == "mickeyCoin" {
            variablePass.mickeyCoinHit++
            print("Number of coins collected: \(variablePass.mickeyCoinHit)")
            touchNode.runAction(SKAction.sequence([coinAddedAction, SKAction.removeFromParent()]))
            //Add a sound when a coin is removed.
            runAction(coinAction)
            //Add one to the players coin score.
        }
        //Check to see if the touch is on the pause button.
        if touchNode.name == "pauseButton" {
            if pauseButtonClicked == false && scene!.view!.paused == false {
                pauseButtonClicked = true
                
                //Create the sprite that will appear when the pause Button is clicked.
                pauseSpriteBackground.zPosition = 4
                pauseSpriteBackground.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)/1.625)
                self.addChild(pauseSpriteBackground)
            } else {
                //Remove the pause screen, and set the game back to un-paused.
                pauseSpriteBackground.removeFromParent()
                pauseButton.texture = SKTexture(imageNamed: "pauseButton")
                pauseButtonClicked = false
                scene!.view!.paused = false
                player.play()
                //Restart the timer, with the number it left off on when the game was paused.
                timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "timerAction", userInfo: nil, repeats: true)
                timerCounter = saveTime
                timerAction()
            }
        }
    }
    
    //Create the function that will load the percentages for the users achievement.
    func loadAchievementPercentages() {
        print("Getting percent of past achievments.")
        //This piece will load the achievement percentage.
        GKAchievement.loadAchievementsWithCompletionHandler({ (allAchievements, error) -> Void in
            //if there is an error, it will be handled in a print statement.
            if error != nil {
                print("Game center could not load achiements with error \(error)")
            } else {
                //If there is no error, continue to load the achievements.
                //This could be nil if there is no progress on any achievments thus far.
                if (allAchievements != nil) {
                    //For each achievement created in the apple developer account...
                    for theAchievement in allAchievements! {
                        //Assign each achievement to be their own achievement, and therefore display them separately.
                        if let singleAchievement:GKAchievement = theAchievement {
                            gameCenterAchievments[singleAchievement.identifier!] = singleAchievement
                        }
                    }
                    //For each individual ID and achievement in the apple developer account...
                    for (id, achievement) in gameCenterAchievments {
                        //Print statements for each achievement along with whether they are complete.
                        print("\(id) - \(achievement.percentComplete)")
                    }
                }
            }
        })
    }
    
    //Create the function that will change the current percentage of achievement.
    func incrementCurrentPercentOfAchievement(identifier: String, amount: Double) {
        
        //Check to make sure the user is signed into Game Center, if they are....
        if GKLocalPlayer.localPlayer().authenticated {
            
            //Create a variable for the current percent.
            var currentPercentFound: Bool = false
            
            //If there are currently achievements available...
            if (gameCenterAchievments.count != 0) {
                //For each ID and achievement in the Game Center Achievements...
                for (id, achievement) in gameCenterAchievments {
                    //if the ID is equal to the identifier...
                    if (id == identifier) {
                        //Change the current percent found to true.
                        currentPercentFound = true
                        //Create a varaiable that will represent the current percentage of an achievement.
                        var currentPercent: Double = achievement.percentComplete
                        //Assign the current percent equal to itself plus an amount.
                        currentPercent = currentPercent + amount
                        //Call the report achievement function.
                        reportAchievement(identifier, percentComplete: currentPercent)
                        
                        break
                        
                    }
                }
            }
            
            //If there is no current percentage found...
            if (currentPercentFound == false) {
                //Call the report achievement function.
                reportAchievement(identifier, percentComplete: amount)
                
            }
        }
    }
    
    //Create the function that will report the achievement and percentage to the leaderboard
    func reportAchievement(identifier: String, percentComplete: Double) {
        //Create a variable that will represent each achievement.
        let achievement = GKAchievement(identifier: identifier)
        //Use the achievement plus the percentage complete of that achievement and assign it to the varable percent complete.
        achievement.percentComplete = percentComplete
        //Create an achievement array which will have all achievements created for the game.
        let achievementArray: [GKAchievement] = [achievement]
        //Report each achievement to the game center.
        GKAchievement.reportAchievements(achievementArray) { (error) -> Void in
            //if there is an error...
            if (error != nil) {
                //Print out the error.
                print(error)
            } else {
                //Otherwise print out the achievement and percentage.
                print("Reported Achievement with percentage complete of \(percentComplete)")
                gameCenterAchievments.removeAll()
                //Then remove and reload all achievements and percentages for the game.
                self.loadAchievementPercentages()
            }
        }
    }
    
    //This function is somthing that I can use in my game
    func spawnMickey() {
        //Create the sprite, which will be the mickeys.
        let sprite: SKSpriteNode = SKSpriteNode(imageNamed: "mickey")
        sprite.name = physicsCategories.mickeyName
        //Because the image is too big for multiple to fit on the screen, shrk the x and y scale to be smaller.
        sprite.xScale = 0.13
        sprite.yScale = 0.13
        //Assign the position of z, or how far back it is, to 1 so that it is not part of the background.
        sprite.zPosition = 1
        
        //This is where you will assign where the mickey appears on the screen? Change the axis that is randomized from the y axis to the x axis.
        
        var xCoords: CGFloat = 0.0
        
        let maxX = CGRectGetMaxX(self.frame)
        let maxY = CGRectGetMaxY(self.frame)
        
        let randomXCoords = [maxX, maxX/2, maxX/3]
        let selectedCoords = arc4random_uniform(UInt32(randomXCoords.count))
        
        if selectedCoords == 0 {
            xCoords = maxX/1.5
        }
        if selectedCoords == 1 {
            xCoords = maxX/2
        }
        if selectedCoords == 2 {
            xCoords = maxX/3
        }
        
        //% = remainder.
        
        //Assign the starting point of the sprite to the top center of the frame.
        sprite.position = CGPointMake(xCoords, maxY)
        
        //Add in physics for the mickey
        sprite.physicsBody = SKPhysicsBody(circleOfRadius: sprite.size.height/2)
        //This is the friction that will affect your object.
        sprite.physicsBody?.friction = 0.0
        //This is the amount of bounciness your object will have.
        sprite.physicsBody?.restitution = 0.0
        //This is how much your object will slow down over time.
        sprite.physicsBody?.linearDamping = 0.0
        //Defines which category this mask belongs to.
        sprite.physicsBody?.categoryBitMask = physicsCategories.mickeyCategory
        //Defines which object or objects this mask interacts with.
        sprite.physicsBody?.contactTestBitMask = physicsCategories.mickeyObstacleCategory
        
        
        //Add the sprite to the scene.
        self.addChild(sprite)
        
        //Create a duration so that the mickeys come aat a constant pace.
        let duration = 1.7
        
        //Move the sprite from the point at the top of the screen to the point at the bottom of the screen.
        let actionMove: SKAction = SKAction.moveTo(CGPointMake(xCoords, -100), duration: duration)
        
        //When the mickey has moved across the screen completely, remove it to save space.
        let done: SKAction = SKAction.removeFromParent()
        
        //Run the action.
        sprite.runAction(SKAction.sequence([actionMove, done]))
    }
    
    func spawnCoin() {
        //Create the sprite, which will be the mickey coins.
        let coinSprite: SKSpriteNode = SKSpriteNode(imageNamed: "mickeyCoin")
        coinSprite.name = "mickeyCoin"
        //Because the image is too big for multiple to fit on the screen, shrink the x and y scale to be smaller.
        coinSprite.xScale = 0.25
        coinSprite.yScale = 0.25
        //Assign the position of z, or how far back it is, to 1 so that it is not part of the background.
        coinSprite.zPosition = 1
        
        //This is where you will assign where the mickey coin appears on the screen? Change the axis that is randomized from the y axis to the x axis.
        
        var xCoords: CGFloat = 0.0
        
        let maxX = CGRectGetMaxX(self.frame)
        let maxY = CGRectGetMaxY(self.frame)
        
        let randomXCoords = [maxX/1.72, maxX/2.5]
        
        let selectedXCoords = arc4random_uniform(UInt32(randomXCoords.count))
        
        if selectedXCoords == 0 {
            xCoords = maxX/1.72
        }
        if selectedXCoords == 1 {
            xCoords = maxX/2.5
        }
        
        //% = remainder.
        
        //Assign the starting point of the sprite to the top center of the frame.
        coinSprite.position = CGPointMake(xCoords, maxY)
        
        //Add the sprite to the scene.
        self.addChild(coinSprite)
        
        //Create a duration so that the mickeys come at a constant pace.
        let duration = 1.5
        
        //Move the sprite from the point at the top of the screen to the point at the bottom of the screen.
        let actionMove: SKAction = SKAction.moveTo(CGPointMake(xCoords,-100), duration: duration)
        
        //When the mickey has moved across the screen completely, remove it to save space.
        let done: SKAction = SKAction.removeFromParent()
        
        //Run the action.
        coinSprite.runAction(SKAction.sequence([actionMove, done]))
        
    }
    
    func spawnObstacle() {
        //Create the sprite, which will be the bombs.
        let obstacleSprite: SKSpriteNode = SKSpriteNode(imageNamed: "mickeyObstacle")
        
        //Because the image is too big for multiple to fit on the screen, shrink the x and y scale to be smaller.
        obstacleSprite.xScale = 0.08
        obstacleSprite.yScale = 0.08
        //Assign the position of z, or how far back it is, to 1 so that it is not part of the background.
        obstacleSprite.zPosition = 1
        
        //This is where you will assign where the mickey bomb appears on the screen? Change the axis that is randomized from the y axis to the x axis.
        
        var xCoords: CGFloat = 0.0
        var yCoords: CGFloat = 0.0
        
        let maxX = CGRectGetMaxX(self.frame)
        let maxY = CGRectGetMaxY(self.frame)
        
        let randomXCoords = [maxX, maxX/2, maxX/3]
        let randomYCoords = [ maxY/4.5, maxY/4.75, maxY/5]
        
        let selectedXCoords = arc4random_uniform(UInt32(randomXCoords.count))
        let selectedYCoords = arc4random_uniform(UInt32(randomYCoords.count))
        
        if selectedXCoords == 0 {
            xCoords = maxX
        }
        if selectedXCoords == 1 {
            xCoords = maxX/2
        }
        if selectedXCoords == 2 {
            xCoords = maxX/3
        }
        
        if selectedYCoords == 0 {
            yCoords = maxY/4.5
        }
        if selectedYCoords == 1 {
            yCoords = maxY/4.75
        }
        if selectedYCoords == 2 {
            yCoords = maxY/5
        }
        
        //% = remainder.
        
        //Assign the starting point of the sprite to the top center of the frame.
        obstacleSprite.position = CGPointMake(xCoords, yCoords)
        
        obstacleSprite.name = physicsCategories.mickeyObstacleName
        
        //Add the sprite to the scene.
        self.addChild(obstacleSprite)
        
        obstacleSprite.physicsBody = SKPhysicsBody(rectangleOfSize: obstacleSprite.frame.size)
        obstacleSprite.physicsBody?.restitution = 0.1
        obstacleSprite.physicsBody?.friction = 0.4
        //The paddle is not dynamic because it does not move when somthing hits it.
        obstacleSprite.physicsBody?.dynamic = false
        obstacleSprite.physicsBody?.categoryBitMask = physicsCategories.mickeyObstacleCategory
        
        //Create a duration so that the mickeys come at a constant pace.
        let duration = 2.0
        
        //Move the sprite from the point at the top of the screen to the point at the bottom of the screen.
        let actionMove: SKAction = SKAction.moveTo(CGPointMake(xCoords,yCoords), duration: duration)
        
        //When the mickey has moved across the screen completely, remove it to save space.
        let done: SKAction = SKAction.removeFromParent()
        
        //Run the action.
        obstacleSprite.runAction(SKAction.sequence([actionMove, done]))
    }
    
    //Create an update function to save space.
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        var timeSinceLast: CFTimeInterval = currentTime - self.lastUpdateTime
        self.lastUpdateTime = currentTime
        
        if (timeSinceLast > 1) {
            timeSinceLast = 1.0/60.0
        }
        
        self.updateWithTimeSinceLast(timeSinceLast)
    }
    
    func updateWithTimeSinceLast(timeSinceLast: CFTimeInterval) {
        self.lastSpawnTime += timeSinceLast
        self.lastObstacleSpawnTime += timeSinceLast
        self.lastCoinSpawnTime += timeSinceLast
        self.globalLabel.text = String(variablePass.mickeyCoinHit)
        
        if  timerCounter < 95 {
            
            if mickeySpawnCount < 175 {
                
                if timerCounter == 57 {
                    incrementCurrentPercentOfAchievement("50PercentLevel3", amount: 100)
                }
                
                if self.lastSpawnTime > 0.38 {
                    
                    self.lastSpawnTime = 0
                    self.spawnMickey()
                    mickeySpawnCount++
                    globalKeyNumber.text = "\(mickeySpawnCount)/175"
                }
                
                if pauseButtonClicked == true {
                    pauseButton.texture = SKTexture(imageNamed: "playButton")
                    scene!.view!.paused = true
                    player.pause()
                    removeAllActions()
                    saveTime = timerCounter
                    timer.invalidate()
                }
                
                if (self.lastCoinSpawnTime > 9) {
                    self.lastCoinSpawnTime = 0
                    self.spawnCoin()
                }
                
                if (self.lastObstacleSpawnTime > 10) {
                    self.lastObstacleSpawnTime = 0
                    self.spawnObstacle()
                }
            }
        } else {
            timer.invalidate()
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            let gameScene = YouWin(size: self.size)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
            
        }
    }
}


